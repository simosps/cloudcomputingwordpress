# **Dockerfile:**

Για την εργασία μας χρησιμοποιήσαμε images απο το dockerHub.

Τα docker images που χρησομοποιήσαμε είναι τα εξής:  
  
  
1) **mariadb:latest** ([https://hub.docker.com/_/mariadb](https://hub.docker.com/_/mariadb "https://hub.docker.com/_/mariadb"))  
2) **phpmyadmin/phpmyadmin:latest** ([https://hub.docker.com/r/phpmyadmin/phpmyadmin](https://hub.docker.com/r/phpmyadmin/phpmyadmin "https://hub.docker.com/r/phpmyadmin/phpmyadmin"))  
3) **wordpress:latest** ([https://hub.docker.com/_/wordpress](https://hub.docker.com/_/wordpress "https://hub.docker.com/_/wordpress"))  



# **Υaml file:**

Το Yaml αρχείο αποτελείται απο 3 services, την βάση , το website και το phpmyadmin ως διαχειριστικό εργαλείο της βάσης.  
Το κάθε service περιέχει:  
1) Το όνομα του host  
2) Το image που χρησιμοποιεί  
3) Τη πόρτα που χρησιμοποιεί (μέσα και έξω απο τον container)

4) Κάποιες βασικές μεταβλητές περιβάλλοντος  με default τιμές

5) Τα volumes που χρησιμοποιεί το κάθε service (το path στο file system του εκάστοτε μηχανήματος και το path μέσα στον container)  
6) Τις παραμέτρους κατά την εκκίνηση του κάθε service, τα προαπαιτούμενα χαρακτηριστικά που θα καταλάβει το service και την πολιτική που θα ακολουθήσει σε περίπτωση αποτυχίας  
7) Σε ποιο node του swarm θα τρέξει το κάθε service  
8) Το δίκτυο που χρησιμοποιεί

# **Μέλη Ομάδας:**

cs141090  
cs141290  
cs141049  
