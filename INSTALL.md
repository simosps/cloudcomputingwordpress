# **How to install:**

Για την εγκατάσταση και εκτέλεση  του εικονικού  εργαστηρίου  αρκεί:  
1) Να φτιάξουμε τους τρεις φακέλους (plugins,uploads,themes) στο μηχάνημα όπου θα τρέξει ο container με το service με του wordpress:  
mkdir plugins   
mkdir themes  
mkdir uploads 

2) Στη συνέχεια να εκτελέσουμε την εντολή για το compose του yaml αρχείου απο το μηχάνημα του manager:  
“docker stack deploy -c docker-compose.yml wordpress”
